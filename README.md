#How to Run Project



After cloning the project run


1. npm install (install packages and dependencies)


2. ng serve  (run the project)


#Resources used and relevant references


1. Angular Material


2. ngx-intl-tel-input package


#Obstacles

The international telephone field input


Solved by installing an Angular package.



#Reason for design 

I used Angular Framework and Angular Material library to build project.
While beaking down the project, I could easily list all dependencies and libraries
that I will need to build project without much research. I could solve the problem
and save time.


#If you have more time, what would you do differently? Also, what would you have 
added additionally


Build the header svg to achieve the curve design
